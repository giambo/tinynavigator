TinyNavigator
==============

Very simple GET-based navigator with fluent interface.

## Example
Just subclass TinyNavigator and add your mappings:

```java
@WebServlet( name = "MessagesServlet", urlPatterns = "/messages/*", loadOnStartup = 1 )
public class MessagesServlet extends TinyNavigator {

    public Mappings createMappings() {
        return MappingsFactory.start()
            .begin()
                .map( "get/id/{id}" )
                .to( MessagesHandler.class, "getById" )
            .end()
            .begin()
                .map( "get/author/{author}" )
                .to( MessagesHandler.class, "getByAuthor" )
            .end()
            .begin()
                .map( "get/forum={forum};author={author}" )
                .to( MessagesHandler.class, "getByForumAuthor" )
            .end()
            .begin()
                .map( "get" )
                .to( MessagesHandler.class, "getAll" )
            .end()
        .build();
    }

}
```

The corresponding Handler class implements the methods with the parameter names:

```java
public String getById( @Param("id") Long id ) throws Exception {
    // get by id
}

public String getByAuthor( @Param("author") String author ) throws Exception {
    // get by author
}
public String getByForumAuthor( @Param("forum") String forum, @Param("author") String author ) throws Exception {
    // get by forum
}

public String getAll() throws Exception {
    // get all
}

```

Obviously you are not limited to one parameter, and the sequence in the Handler method must not be the same as the one of the URL (@Param annotation).

The handler methods accept parameters of type String, Long and Integer out of the box. Converters for other types can be defined as implementations of IParamConverter:

```java
new MappingsFactory.start()
    .converters(
        new IParamConverter<Byte>() {
            @Override
            public Class<Byte> type() {
                return Byte.class;
            }

            @Override
            public Byte convert( String paramValue ) {
                return Byte.parseByte( paramValue );
            }
        },
        new IParamConverter<Boolean>() {
            @Override
            public Class<Boolean> type() {
                return Boolean.class;
            }

            @Override
            public Boolean convert( String paramValue ) {
                return Boolean.parseBoolean( paramValue );
            }
        }
    )
    .begin()
        .map( "byte={byte}&&boolean={bool}" )
        .to( HandlerClass.class, "getByteAndBoolean" )
    .end()
.build();
```

## Running the demo
If you have grails installed, you can run the demo
```
gradle appRun
```
Then open you browser at the address suggested (http://localhost:8080/TinyNavigator)
