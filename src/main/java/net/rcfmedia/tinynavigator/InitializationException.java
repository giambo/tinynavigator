package net.rcfmedia.tinynavigator;

public class InitializationException extends RuntimeException {
    public InitializationException( String message ) {
        super( message );
    }
}
