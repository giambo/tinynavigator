package net.rcfmedia.tinynavigator;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.rcfmedia.tinynavigator.mapping.Mappings;

public abstract class TinyNavigator extends HttpServlet {

    private Mappings mappings;

    public abstract Mappings createMappings();

    @Override
    public void init() throws ServletException {
        mappings = createMappings();
    }

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
        String requestURI = req.getRequestURI();
        String pathInfo = req.getPathInfo();
        String relevantPart = requestURI.substring( requestURI.indexOf( pathInfo ) );
        res.getWriter().write( mappings.callFor( relevantPart ) );
    }

}
