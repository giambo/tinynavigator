package net.rcfmedia.tinynavigator.demo;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet( name = "Demos", urlPatterns = "/", loadOnStartup = 1 )
public class Demos extends HttpServlet {

    @Override
    protected void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
        res.getWriter().write( "<html><body>"
            + "<h3>PlanetDemo</h3>"
            + "<ul><li><a href='PlanetsDemo/gePlanets;sortedAsc=yes'>Sorted</a>"
            + "<li><a href='PlanetsDemo/gePlanets;sortedAsc=no'>Unsorted</a></li>" );
    }

}
