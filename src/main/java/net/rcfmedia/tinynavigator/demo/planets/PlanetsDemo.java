package net.rcfmedia.tinynavigator.demo.planets;

import javax.servlet.annotation.WebServlet;
import net.rcfmedia.tinynavigator.TinyNavigator;
import net.rcfmedia.tinynavigator.mapping.Mappings;
import net.rcfmedia.tinynavigator.mapping.MappingsFactory;
import net.rcfmedia.tinynavigator.mapping.param.IParamConverter;

@WebServlet( name = "PlanetsDemo", urlPatterns = "/PlanetsDemo/*", loadOnStartup = 1 )
public class PlanetsDemo extends TinyNavigator {

    @Override
    public Mappings createMappings() {
        return MappingsFactory.start()
            .converters( new IParamConverter<Boolean>() {
                public Class<Boolean> type() {
                    return Boolean.class;
                }

                public Boolean convert( String paramValue ) {
                    return "yes".equals( paramValue );
                }
            } )
            .begin()
                .map( "/gePlanets;sortedAsc={sortedAsc}" )
                .to( PlanetsHandler.class, "getPlanets" )
            .end()
            .build();
    }
}
