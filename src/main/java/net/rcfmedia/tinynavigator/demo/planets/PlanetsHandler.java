package net.rcfmedia.tinynavigator.demo.planets;

import java.util.Collections;
import java.util.List;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import net.rcfmedia.tinynavigator.mapping.param.Param;

public class PlanetsHandler {

    public String getPlanets( @Param( "sortedAsc" ) Boolean sortedAsc ) {
        List<String> planets = Lists.newArrayList(
            "Mercury",
            "Venus",
            "Earth",
            "Mars",
            "Jupiter",
            "Saturn",
            "Uranus",
            "Neptune"
            // sorry Pluto !
        );
        if( sortedAsc ) {
            Collections.sort( planets );
        }
        return Joiner.on( "," ).join( planets );
    }

}
