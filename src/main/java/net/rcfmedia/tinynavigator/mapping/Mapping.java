package net.rcfmedia.tinynavigator.mapping;

import java.lang.reflect.Method;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import com.google.common.base.MoreObjects;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import net.rcfmedia.tinynavigator.InitializationException;
import net.rcfmedia.tinynavigator.mapping.param.MethodParamDefinitions;
import net.rcfmedia.tinynavigator.mapping.param.ParamConverter;

class Mapping {

    private static LoadingCache<Class, Object> handlersCache = CacheBuilder.newBuilder()
        .build(
            new CacheLoader<Class, Object>() {
                @Override
                public Object load( Class clazz ) throws Exception {
                    return clazz.newInstance();
                }
            }
        );

    private String uriMap;
    private Class handlerClass;
    private Method method;
    private ParamConverter paramConverter;

    private String regexpURI;
    private MethodParamDefinitions methodParamDefs;
    private List<String> uriParamNames;

    Mapping( ParamConverter paramConverter) {
        this.paramConverter = paramConverter;
    }

    /**
     * Checks the consistency of this mapping
     */
    protected void check() {
        if( null == uriMap ) {
            throw new InitializationException( "URI uriMap undefined" );
        }
        if( null == handlerClass ) {
            throw new InitializationException( "Handler class undefined" );
        }
        if( null == method ) {
            throw new InitializationException( "Handler method undefined" );
        }

        regexpURI = MappingHelper.createRegexpURI( uriMap );
        methodParamDefs = MappingHelper.getMethodParams( method );
        uriParamNames = MappingHelper.getURIParamNames( uriMap );

        if( methodParamDefs.size() != uriParamNames.size() ) {
            throw new InitializationException( "Number of params in URI '" + uriMap + "' don't match number of params in method '" + method.getName() + "'" );
        }
    }

    /**
     * Call the handler associated with this URI
     *
     * @param requestURI
     * @return
     * @throws ServletException
     */
    String call( String requestURI ) throws ServletException {
        try {
            Object handler = handlersCache.get( handlerClass );
            Object[] arguments = createArguments( requestURI );
            return (String) method.invoke( handler, arguments );
        } catch( Exception e ) {
            throw new ServletException( e );
        }
    }

    /**
     * Extract all the parameter values from the supplied requestURI for the supplied {code}Mapping{code}
     *
     * @param requestURI
     * @return
     */
    Object[] createArguments( String requestURI ) {

        Object[] ret = new Object[ methodParamDefs.size() ];

        Matcher matcher = Pattern.compile( getRegexpURI() ).matcher( requestURI );
        if( matcher.find() ) {
            for( int groupNo = 1; groupNo <= matcher.groupCount(); groupNo++ ) {
                String paramValue = matcher.group( groupNo );
                String paramName = uriParamNames.get( groupNo - 1 );
                int methodParamIndex = methodParamDefs.indexOf( paramName );
                Class paramType = methodParamDefs.classOfParam( paramName );
                ret[ methodParamIndex ] = paramConverter.convert( paramValue, paramType );
            }
        }

        return ret;
    }

    boolean matches( String requestUri ) {
        return Pattern.compile( regexpURI ).matcher( requestUri ).find();
    }

    void setUriMap( String uriMap ) {
        this.uriMap = uriMap;
    }

    void setHandlerClass( Class handlerClass ) {
        this.handlerClass = handlerClass;
    }

    void setMethod( Method method ) {
        this.method = method;
    }

    String getRegexpURI() {
        return regexpURI;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper( this.getClass() )
            .add( "uriMap", uriMap )
            .add( "handlerClass", handlerClass )
            .add( "method", method == null ? null : method.getName() )
            .add( "regexpURI", regexpURI )
            .toString();
    }
}
