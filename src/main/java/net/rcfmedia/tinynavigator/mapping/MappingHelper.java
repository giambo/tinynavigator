package net.rcfmedia.tinynavigator.mapping;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.google.common.collect.Lists;
import net.rcfmedia.tinynavigator.InitializationException;
import net.rcfmedia.tinynavigator.mapping.param.MethodParamDefinitions;
import net.rcfmedia.tinynavigator.mapping.param.Param;

class MappingHelper {

    private static final Pattern PATTERN_MAP = Pattern.compile( "\\{\\s*(.*?)\\s*\\}" );
    private static final String REPLACE_MAP = "([^/]*)";

    /**
     * From the supplied URI map, create a regexp that matches all the parameters.
     *
     * @param uriMap
     * @return
     */
    static String createRegexpURI( String uriMap ) {
        Matcher matcher = PATTERN_MAP.matcher( uriMap );
        while( matcher.find() ) {
            uriMap = matcher.replaceFirst( REPLACE_MAP );
            matcher = PATTERN_MAP.matcher( uriMap );
        }
        return uriMap;
    }


    /**
     * Get all parameters defined by {code}Param{code} in the supplied method.
     *
     * @param method
     * @return
     */
    static MethodParamDefinitions getMethodParams( Method method ) {
        MethodParamDefinitions ret = new MethodParamDefinitions();
        int paramPos = 0;
        for( Annotation[] paramAnnotation : method.getParameterAnnotations() ) {
            String paramName = null;
            for( Annotation annotation : paramAnnotation ) {
                if( annotation instanceof Param ) {
                    paramName = ( (Param) annotation ).value();
                }
            }

            if( null != paramName ) {
                Class paramType = method.getParameterTypes()[ paramPos ];
                ret.add( paramName, paramType );
            } else {
                throw new InitializationException( "No annotation @net.rcfmedia.tinynavigator.Param found for argument at position " + paramPos +
                    " of '" + method.getDeclaringClass().getName() + "#" + method.getName() + "'" );
            }
            paramPos++;
        }

        return ret;
    }


    /**
     * From the supplied URI map, extract all defined parameter names.
     * The sequence in the list reflects the sequence in the URI map.
     *
     * @param uriMap
     * @return
     */
    static List<String> getURIParamNames( String uriMap ) {
        List<String> ret = Lists.newArrayList();
        Matcher matcher = PATTERN_MAP.matcher( uriMap );
        while( matcher.find() ) {
            ret.add( matcher.group( 1 ) );
        }
        return Collections.unmodifiableList( ret );
    }



}
