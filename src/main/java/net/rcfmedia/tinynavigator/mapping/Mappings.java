package net.rcfmedia.tinynavigator.mapping;

import java.util.List;
import javax.servlet.ServletException;
import com.google.common.base.Joiner;

public class Mappings {

    private final List<Mapping> mappings;

    Mappings( List<Mapping> mappings ) {
        this.mappings = mappings;
    }

    public final String callFor( String requestURI ) throws ServletException {
        for( Mapping mapping : mappings ) {
            if( mapping.matches( requestURI ) ) {
                return mapping.call( requestURI );
            }
        }

        throw new ServletException( "No mapping defined for '" + requestURI + "'. Check your mappings definition." );
    }


    @Override
    public String toString() {
        return Joiner.on( '\n' ).join( mappings );
    }
}
