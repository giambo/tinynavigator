package net.rcfmedia.tinynavigator.mapping;

import java.lang.reflect.Method;
import java.util.List;
import com.google.common.collect.Lists;
import net.rcfmedia.tinynavigator.InitializationException;
import net.rcfmedia.tinynavigator.mapping.param.IParamConverter;
import net.rcfmedia.tinynavigator.mapping.param.ParamConverter;
import net.rcfmedia.tinynavigator.mapping.steps.BeginStep;
import net.rcfmedia.tinynavigator.mapping.steps.MapStep;
import net.rcfmedia.tinynavigator.mapping.steps.StartEndStep;
import net.rcfmedia.tinynavigator.mapping.steps.ToStep;

class MappingsBuilder implements StartEndStep, BeginStep, MapStep, ToStep {

    private Mapping actualMapping;
    private List<Mapping> mappings = Lists.newArrayList();
    private ParamConverter paramConverter = new ParamConverter();

    public MappingsBuilder converters( IParamConverter... converters ) {
        for( IParamConverter converter : converters ) {
            paramConverter.register( converter );
        }

        return this;
    }

    public BeginStep begin() {
        actualMapping = new Mapping( paramConverter );
        return this;
    }

    public MapStep map( String map ) {
        actualMapping.setUriMap( map );
        return this;
    }

    public ToStep to( Class handlerClass, String methodName ) {
        Method method = null;
        for( Method m : handlerClass.getMethods() ) {
            if( m.getName().equals( methodName ) && m.getReturnType() == String.class ) {
                method = m;
            }
        }
        if( null == method ) {
            throw new InitializationException( "Cannot find method '" + methodName + "' in '" + handlerClass.getName() + "' returning a String" );
        }
        actualMapping.setHandlerClass( handlerClass );
        actualMapping.setMethod( method );

        return this;
    }

    public StartEndStep end() {

        actualMapping.check();
        mappings.add( actualMapping );


        return this;
    }

    public Mappings build() {
        return new Mappings( mappings );
    }

}
