package net.rcfmedia.tinynavigator.mapping;

import net.rcfmedia.tinynavigator.mapping.steps.StartEndStep;

public class MappingsFactory {

    public static final StartEndStep start() {
        return new MappingsBuilder();
    }

}
