package net.rcfmedia.tinynavigator.mapping.param;

public interface IParamConverter<T> {

    public Class<T> type();

    public T convert( String paramValue );

}
