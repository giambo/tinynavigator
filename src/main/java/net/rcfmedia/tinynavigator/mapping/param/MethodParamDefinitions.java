package net.rcfmedia.tinynavigator.mapping.param;

import java.util.List;
import com.google.common.collect.Lists;
import net.rcfmedia.tinynavigator.InitializationException;

public class MethodParamDefinitions {

    private List<String> paramNames;
    private List<Class> paramTypes;

    public MethodParamDefinitions() {
        paramNames = Lists.newArrayList();
        paramTypes = Lists.newArrayList();
    }

    public void add( String paramName, Class paramType ) {
        if( null == paramName ) {
            throw new InitializationException( "paramName is null" );
        }
        if( null == paramType ) {
            throw new InitializationException( "paramType for paramName '" + paramName + "' is null" );
        }
        paramNames.add( paramName );
        paramTypes.add( paramType );
    }

    public int size() {
        return paramNames.size();
    }

    public int indexOf( String paramName ) {
        return paramNames.indexOf( paramName );
    }

    public Class classOfParam( String paramName ) {
        return paramTypes.get( indexOf( paramName ) );
    }

}
