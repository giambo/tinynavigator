package net.rcfmedia.tinynavigator.mapping.param;

import java.util.Map;
import com.google.common.collect.Maps;

public class ParamConverter {

    private Map<Class, IParamConverter> paramMap = Maps.newHashMap();

    public ParamConverter() {
        paramMap.put( String.class, new IParamConverter<String>() {
            public Class<String> type() {
                return String.class;
            }

            public String convert( String paramValue ) {
                return paramValue;
            }
        } );
        paramMap.put( Long.class, new IParamConverter<Long>() {
            public Class<Long> type() {
                return Long.class;
            }

            public Long convert( String paramValue ) {
                return Long.parseLong( paramValue );
            }
        } );
        paramMap.put( Integer.class, new IParamConverter<Integer>() {
            public Class<Integer> type() {
                return Integer.class;
            }

            public Integer convert( String paramValue ) {
                return Integer.parseInt( paramValue );
            }
        } );
    }


    public <T> T convert( String paramValue, Class<T> paramType ) {
        IParamConverter<T> converter = paramMap.get( paramType );
        if( null == converter ) {
            return null;
        }
        return converter.convert( paramValue );
    }

    public <T> IParamConverter register( IParamConverter<T> paramConverter ) {
        return paramMap.put( paramConverter.type(), paramConverter );
    }

}
