package net.rcfmedia.tinynavigator.mapping.steps;

public interface BeginStep {

    MapStep map( String map );
}
