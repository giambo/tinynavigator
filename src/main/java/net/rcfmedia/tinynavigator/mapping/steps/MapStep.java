package net.rcfmedia.tinynavigator.mapping.steps;

public interface MapStep {

    ToStep to( Class handlerClass, String methodName );
}
