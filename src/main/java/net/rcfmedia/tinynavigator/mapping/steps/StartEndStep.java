package net.rcfmedia.tinynavigator.mapping.steps;

import net.rcfmedia.tinynavigator.mapping.Mappings;
import net.rcfmedia.tinynavigator.mapping.param.IParamConverter;

public interface StartEndStep {

    BeginStep begin();
    Mappings build();
    StartEndStep converters( IParamConverter... converters );

}
