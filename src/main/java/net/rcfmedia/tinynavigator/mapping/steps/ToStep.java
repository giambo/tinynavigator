package net.rcfmedia.tinynavigator.mapping.steps;

public interface ToStep {

    StartEndStep end();

}
