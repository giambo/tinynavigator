package net.rcfmedia.tinynavigator;


import com.google.common.base.Joiner;
import net.rcfmedia.tinynavigator.mapping.param.Param;

public class TestHandlerClass {

    public String testSequenceOK_1() {
        return "testSequenceOK_1";
    }

    public String testSequenceOK_2() {
        return "testSequenceOK_2";
    }

    public String testOutOfOrderParams( @Param( "author" ) String a, @Param( "forum" ) String forum, @Param( "id" ) Long messageId ) {
        return String.format( "get/forum/%s&id=%d;author=%s", forum, messageId, a );
    }

    public Long testCreateArguments_1( @Param( "id" ) Long id ) {
        return id;
    }

    public String testCreateArguments_2( @Param( "author" ) String author, @Param( "id" ) Long id, @Param( "nextnext" ) String reallyNext, @Param( "next" ) String nextParam ) {
        return Joiner.on( ':' ).join( author, id.toString(), nextParam, reallyNext );
    }

    public void testgetMethodParams_1() {
    }

    public Long testgetMethodParams_2( @Param( "id" ) Long id, @Param( "author" ) String author ) {
        return id;
    }

    public Long testgetMethodParams_3( @Param( "id" ) Long id, String author ) {
        return id;
    }

    public void testgetMethodParams_4( @SuppressWarnings( "all" ) @Param( "id" ) Long id, @Param( "author" ) String author ) {

    }

    public String testConverters( @Param( "bool" ) Boolean bool, @Param( "byte" ) Byte b ) {
        return Joiner.on( ',' ).join( b.getClass().getSimpleName(), b.toString(), bool.getClass().getSimpleName(), bool.toString() );
    }
}