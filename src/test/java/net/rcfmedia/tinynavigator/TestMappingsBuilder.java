package net.rcfmedia.tinynavigator;

import org.junit.Assert;
import org.junit.Test;
import net.rcfmedia.tinynavigator.mapping.Mappings;
import net.rcfmedia.tinynavigator.mapping.MappingsFactory;
import net.rcfmedia.tinynavigator.mapping.param.IParamConverter;

public class TestMappingsBuilder {

    @Test
    public void testSequenceOK_1() {
        Mappings mappings = MappingsFactory.start()
            .begin()
                .map( "one" )
                .to( TestHandlerClass.class, "testSequenceOK_1" )
            .end()
            .begin()
                .map( "two" )
                .to( TestHandlerClass.class, "testSequenceOK_2" )
            .end()
        .build();

        Assert.assertEquals(
            "Mapping{uriMap=one, handlerClass=class net.rcfmedia.tinynavigator.TestHandlerClass, method=testSequenceOK_1, regexpURI=one}\n" +
                "Mapping{uriMap=two, handlerClass=class net.rcfmedia.tinynavigator.TestHandlerClass, method=testSequenceOK_2, regexpURI=two}",
            mappings.toString() );

    }

    @Test
    public void testSequenceOK_2() {
        Mappings mappings = MappingsFactory.start()
            .build();
    }

    @Test
    public void testOutOfOrderParams() throws Exception {
        Mappings mappings = MappingsFactory.start()
            .begin()
                .map( "get/forum/{forum}&id={id};author={author}" )
                .to( TestHandlerClass.class, "testOutOfOrderParams" )
            .end()
        .build();

        String ret = mappings.callFor( "get/forum/niceForum&id=42;author=troll" );
        Assert.assertEquals( "get/forum/niceForum&id=42;author=troll", ret );
    }

    @Test
    public void testConverters() throws Exception {
        Mappings mappings = MappingsFactory.start()
            .converters(
                new IParamConverter<Byte>() {
                    public Class<Byte> type() {
                        return Byte.class;
                    }
                    public Byte convert( String paramValue ) {
                        return Byte.parseByte( paramValue );
                    }
                },
                new IParamConverter<Boolean>() {
                    public Class<Boolean> type() {
                        return Boolean.class;
                    }
                    public Boolean convert( String paramValue ) {
                        return Boolean.parseBoolean( paramValue );
                    }
                }
            )
            .begin()
                .map( "byte={byte}&&boolean={bool}" )
                .to( TestHandlerClass.class, "testConverters" )
            .end()
        .build();

        String ret = mappings.callFor( "byte=12&&boolean=true" );
        Assert.assertEquals( "Byte,12,Boolean,true", ret );

    }

}
