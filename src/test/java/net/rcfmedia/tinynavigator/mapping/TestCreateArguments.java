package net.rcfmedia.tinynavigator.mapping;

import java.lang.reflect.Method;
import org.junit.Assert;
import org.junit.Test;
import net.rcfmedia.tinynavigator.TestHandlerClass;
import net.rcfmedia.tinynavigator.mapping.param.ParamConverter;

public class TestCreateArguments {

    @Test
    public void testCreateArguments_1() throws Exception {

        Mapping mapping = new Mapping( new ParamConverter() );
        mapping.setUriMap( "messages/get/id/{id}" );
        mapping.setHandlerClass( TestHandlerClass.class );
        Method handlerMethod = TestHandlerClass.class.getMethod( "testCreateArguments_1", Long.class );
        mapping.setMethod( handlerMethod );
        mapping.check();

        Object[] res = mapping.createArguments( "/test/messages/get/id/12" );
        Assert.assertEquals( 1, res.length );
        Assert.assertEquals( 12L, res[ 0 ] );

        Object invoke = handlerMethod.invoke( TestHandlerClass.class.newInstance(), res );
        Assert.assertEquals( 12L, invoke );
    }

    @Test
    public void testCreateArguments_2() throws Exception {

        Mapping mapping = new Mapping( new ParamConverter() );
        mapping.setUriMap( "messages/get/author/{author}/quack_{id}-1/prot/{next};{nextnext}" );
        mapping.setHandlerClass( TestHandlerClass.class );
        Method handlerMethod = TestHandlerClass.class.getMethod( "testCreateArguments_2", String.class, Long.class, String.class, String.class );
        mapping.setMethod( handlerMethod );
        mapping.check();

        Object[] res = mapping.createArguments( "/test/messages/get/author/Pippo/quack_42-1/prot/anotherParam;next" );
        Assert.assertEquals( 4, res.length );
        Assert.assertEquals( "Pippo", res[ 0 ] );
        Assert.assertEquals( 42L, res[ 1 ] );
        Assert.assertEquals( "next", res[ 2 ] );
        Assert.assertEquals( "anotherParam", res[ 3 ] );

        Object invoke = handlerMethod.invoke( TestHandlerClass.class.newInstance(), res );
        Assert.assertEquals( "Pippo:42:anotherParam:next", invoke );

    }

}
