package net.rcfmedia.tinynavigator.mapping;

import org.junit.Assert;
import org.junit.Test;

public class TestCreateRegexpURI {

    @Test
    public void testCreateRegexp_1() {
        String res = MappingHelper.createRegexpURI( "messages/get/id/{id}/" );
        Assert.assertEquals( "messages/get/id/([^/]*)/", res );
    }

    @Test
    public void testCreateRegexp_2() {
        String res = MappingHelper.createRegexpURI( "messages/get/author/{author}/quack_{id}-1/prot" );
        Assert.assertEquals( "messages/get/author/([^/]*)/quack_([^/]*)-1/prot", res );
    }

    @Test
    public void testCreateRegexp_3() {
        String res = MappingHelper.createRegexpURI( "messages/get" );
        Assert.assertEquals( "messages/get", res );
    }

}
