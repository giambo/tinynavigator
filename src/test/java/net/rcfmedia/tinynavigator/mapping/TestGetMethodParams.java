package net.rcfmedia.tinynavigator.mapping;

import org.junit.Assert;
import org.junit.Test;
import net.rcfmedia.tinynavigator.InitializationException;
import net.rcfmedia.tinynavigator.TestHandlerClass;
import net.rcfmedia.tinynavigator.mapping.param.MethodParamDefinitions;

public class TestGetMethodParams {

    @Test
    public void testgetMethodParams_1() throws Exception {
        MethodParamDefinitions res = MappingHelper.getMethodParams( TestHandlerClass.class.getMethod( "testgetMethodParams_1" ) );
        Assert.assertEquals( 0, res.size() );
    }

    @Test
    public void testgetMethodParams_2() throws Exception {
        MethodParamDefinitions res = MappingHelper.getMethodParams( TestHandlerClass.class.getMethod( "testgetMethodParams_2", Long.class, String.class ) );
        Assert.assertEquals( 2, res.size() );
        Assert.assertEquals( 0, res.indexOf( "id" ) );
        Assert.assertEquals( Long.class, res.classOfParam( "id" ) );
        Assert.assertEquals( 1, res.indexOf( "author" ) );
        Assert.assertEquals( String.class, res.classOfParam( "author" ) );
    }

    @Test( expected = InitializationException.class )
    public void testgetMethodParams_3() throws NoSuchMethodException {
        MappingHelper.getMethodParams( TestHandlerClass.class.getMethod( "testgetMethodParams_3", Long.class, String.class ) );
    }

    @Test
    public void testgetMethodParams_4() throws Exception {
        MethodParamDefinitions res = MappingHelper.getMethodParams( TestHandlerClass.class.getMethod( "testgetMethodParams_4", Long.class, String.class ) );
        Assert.assertEquals( 2, res.size() );
        Assert.assertEquals( 0, res.indexOf( "id" ) );
        Assert.assertEquals( Long.class, res.classOfParam( "id" ) );
        Assert.assertEquals( 1, res.indexOf( "author" ) );
        Assert.assertEquals( String.class, res.classOfParam( "author" ) );
    }

}
