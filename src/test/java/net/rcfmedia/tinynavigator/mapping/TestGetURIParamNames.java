package net.rcfmedia.tinynavigator.mapping;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;

public class TestGetURIParamNames {

    @Test
    public void tesGetURIParamNames_1() {
        List<String> res = MappingHelper.getURIParamNames( "messages/get/id/{id}/" );
        Assert.assertEquals( 1, res.size() );
        Assert.assertEquals( "id", res.get( 0 ) );
    }

    @Test
    public void testCreateRegexp_2() {
        List<String> res = MappingHelper.getURIParamNames( "messages/get/author/{author}/quack_{id}-1/prot/{next};{nextnext}" );
        Assert.assertEquals( 4, res.size() );
        Assert.assertEquals( "author", res.get( 0 ) );
        Assert.assertEquals( "id", res.get( 1 ) );
        Assert.assertEquals( "next", res.get( 2 ) );
        Assert.assertEquals( "nextnext", res.get( 3 ) );
    }

    @Test
    public void testCreateRegexp_3() {
        List<String> res = MappingHelper.getURIParamNames( "messages/get" );
        Assert.assertEquals( 0, res.size() );
    }

}
